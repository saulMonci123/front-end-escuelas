import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

interface IPayloda{
    _id: string,
    iat:number,
    exp:number

}

/**
 * Middleware que valida que exista el token en la peticion que realiza el cliente
 * @param req 
 * @param res 
 * @param next 
 */
export const tokenValidation = (req:Request, res:Response, next:NextFunction) => {
    try{
        const token = req.header('auth-token');
        if(!token) return res.status(401).json('Access denied');
    
        const payload = jwt.verify(token, <string>process.env.APP_TOKEN) as IPayloda;
    
        req.userId = payload._id;
    
        next();
    }catch(e){
        res.status(400).json(e);
    }
    
};