import paginate from "jw-paginate";


export default class Utils{

    public async pagination(data:any, req:any){
        const items = data;

        // get page from query params or default to first page
        const page = parseInt(req.query.page) || 1;

        // get pager object for specified page
        const pager = await paginate(items.length, page, 10);

        // get page of items from items array
        const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

        return {
            pager,
            data:pageOfItems
        }
    }

    
}

export  const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}
