import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class initalMigration1605640703547 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name:"User",
            columns:[
                {
                    name: "id",
                    type: "int",
                    isPrimary: true
                },
                {
                    name: "username",
                    type: "varchar",
                },
                {
                    name: "password",
                    type: "varchar",
                },
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
