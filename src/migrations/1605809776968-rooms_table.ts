import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class roomsTable1605809776968 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.createTable(new Table({
            name:"clientes",
            columns:[
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                },
                {
                    name:"nombre",
                    type:"varchar"
                },
                {
                    name:"tipo",
                    type:"varchar"
                }
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
