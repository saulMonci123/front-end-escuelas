import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createClientsTable1605725313753 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.createTable(new Table({
            name:"clientes",
            columns:[
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                },
                {
                    name:"nombre",
                    type:"varchar"
                },
                {
                    name:"zona",
                    type:"varchar"
                }
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
