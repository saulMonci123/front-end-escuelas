import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class roomsTypeTable1605811894395 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.createTable(new Table({
            name:"tipoHabitaciones",
            columns:[
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                },
                {
                    name:"nombre",
                    type:"varchar"
                }
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
