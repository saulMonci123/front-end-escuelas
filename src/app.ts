import express, {Application} from "express";

import morgan from 'morgan';
import cors from 'cors';
import 'reflect-metadata';
const app: Application = express();





//settings
app.set('port', 3000);



//middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());



export default app;