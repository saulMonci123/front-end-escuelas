import { Router } from "express";
import AlumnsController from '../../controllers/alumns.controller';
const router: Router = Router();
const alumnsController = new AlumnsController();


/**
 * Ruta para dar de alta un usuario
 */
router.post('/getAlumn', (req, res, next) =>{
    alumnsController.getAlumn(req, res, next);
});


export default router;