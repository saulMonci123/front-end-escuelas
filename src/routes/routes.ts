import express from 'express';
//Routes
import userRoutes from './user/user';
import authRoutes from './auth/auth';
import alumnsRoutes from "./alumns/alumns.routes"
import conceptsRoutes from "./nivels/concepts.routes"
import {tokenValidation} from '../libs/validateToken';
/**
 * Se instancea el ruteador para añadir las rutas necesarias al servidor
 */
const router = express.Router();

/**
 * Rutas declaradas por controladores
 */

router.use('/api/auth', authRoutes);






/**
 * Middleware para verificar que el cliente envia el token
 */
router.use(tokenValidation);

router.use('/api/user', userRoutes);
router.use('/api/alumns', alumnsRoutes )
router.use('/api/concepts', conceptsRoutes);
export default router;


