import { Router } from "express";
import AuthController from '../../controllers/auth.controller';
import {tokenValidation} from '../../libs/validateToken';
const router: Router = Router();
const authController = new AuthController();


/**
 * Ruta para dar de alta un usuario
 */
router.post('/signup', (req, res, next) =>{
    authController.signup(req, res, next);
});


/**
 * Ruta para loggear un usuario
 */
router.post('/signin', (req, res, next) =>{
    authController.signin(req, res, next);
});

router.get('/profile', tokenValidation ,(req, res, next) =>{
    authController.profile(req, res, next);
});
export default router;