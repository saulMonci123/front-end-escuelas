import { Router } from "express";
import UserController from '../../controllers/user.controller';
const router: Router = Router();
const userController = new UserController();

router.get('/', (req, res, next) =>{
    userController.index(req, res, next);
});

// router.get('/users', (req, res) =>{
//     res.send('Hola');
// });

// router.get('/users', (req, res) =>{
//     res.send('Hola');
// });

// router.get('/users', (req, res) =>{
//     res.send('Hola');
// });

// router.get('/users', (req, res) =>{
//     res.send('Hola');
// });
export default router;