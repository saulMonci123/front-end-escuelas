import { Router } from "express";
import ConceptsController from '../../controllers/concepts.controller';
const router: Router = Router();
const conceptsController = new ConceptsController();


/**
 * Ruta para dar de alta un usuario
 */
router.get('/', (req, res, next) =>{
    conceptsController.getNivels(req, res, next);
});


router.post('/makePay',(req, res, next)=>{
    conceptsController.generatePay(req, res, next);
});

router.get('/getTypePays', (req, res, next) =>{
    conceptsController.getTypesPays(req, res, next);
});


export default router;