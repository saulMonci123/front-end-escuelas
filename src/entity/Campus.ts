import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Campus extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column({
        type:"int"
    })
    id_empresa: number;

    @Column({
        type:"varchar"
    })
    campus: string;

    


}