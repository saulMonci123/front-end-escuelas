import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class PagosDetalles extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type:"int"
    })
    id_pago: number;

    @Column({
        type:"varchar",
        nullable:true
    })
    ciclo: string;

    @Column({
        type:"integer",
        nullable:true
    })
    anio: number;

    @Column({
        type:"integer",
        nullable:true
    })
    mes: number;

    @Column({
        type:"integer"
    })
    id_concepto: number;

    @Column({
        type:"integer",
        nullable:true
    })
    monto_original: number;
    
    @Column({
        type:"numeric"
    })
    monto_paga: number;

    @Column({
        type:"integer",
        nullable:true
    })
    ajuste_id_user: number;

    @Column({
        type:"integer"
    })
    id_tipopago: number;





}