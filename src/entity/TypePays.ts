import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class TiposPagos extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column({
        type:"varchar"
    })
    tipo_pago: string;

    @Column({
        type:"varchar"
    })
    descripcion: string;
}