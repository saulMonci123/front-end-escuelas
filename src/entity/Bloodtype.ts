import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Tiposangre extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column({
        type:"varchar"
    })
    grupo: string;

}