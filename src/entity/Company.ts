import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Empresa extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column({
        type:"varchar"
    })
    rfc: string;

    @Column({
        type:"varchar"
    })
    razon_social: string;

    @Column({
        type:"varchar"
    })
    direccion1: string;

    @Column({
        type:"varchar"
    })
    direccion2: string;

    @Column({
        type:"varchar"
    })
    cp: string;

    @Column({
        type:"varchar"
    })
    email: string;

    @Column({
        type:"varchar"
    })
    telefono: string;

    
}