import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Conceptos extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column()
    cuenta_contable: string;

    @Column()
    escolaridad: string;

    @Column()
    concepto: string;
    
    @Column()
    descripcion: string;

    @Column()
    cuenta_mayor: Number;

    @Column()
    importe: Number;

    @Column()
    porcentaje_descuento: Number;

    @Column()
    genera_recargos: Boolean;

    @Column()
    cuota_recargos: Number;

    @Column({
        type:"date",
        nullable:true,
        default:null
    })
    fecha_limite_descuento: string;

    @Column()
    cuota_descuento: Number;
    
    @Column()
    escalculado: Boolean;

    @Column({
        nullable:true,
        default:null
    })
    fecha_limite_recargos: Date;

    @Column()
    activo: Boolean;


}