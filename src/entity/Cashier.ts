import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Cajeros extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column({
        type:"integer"
    })
    id_usuario: number;

    @Column({
        type:"integer",
        nullable:true
    })
    recibo: number;

    @Column({
        type:"boolean",
        default:true
    })
    status: boolean;
    










}