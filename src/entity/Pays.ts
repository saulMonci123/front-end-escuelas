import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Pagos extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type:"int"
    })
    recibo: number;

    @Column({
        type:"int"
    })
    id_alumno: number;
      

    @Column({
        type:"varchar",
        nullable:true
    })
    referencia: string;

    @Column({
        type:"varchar",
        nullable:true
    })
    cuenta_bancaria: string;

    @Column({
        type:"date"
    })
    fecha_pago: Date;

    @Column({
        type:"varchar",
        nullable:true
    })
    notas: string;

    @Column({
        type:"boolean",
        default:true
    })
    estatus: boolean;


    @Column({
        type:"int"
    })
    id_user: number;

    @Column({
        type:"int",
        nullable:true
    })
    id_empresa: number;

    @Column({
        type:"int",
        nullable:true
    })
    id_campus: number;

    @Column({
        type:"int",
        nullable:true
    })
    id_nivel: number;

    @Column({
        type:"varchar",
        nullable:true
    })
    motivo_cancelado: string;

    @Column({
        type:"date",
        nullable:true
    })
    fecha_cancelacion: Date;

    @Column({
        type:"int",
        nullable:true
    })
    id_user_cancela: number;
}