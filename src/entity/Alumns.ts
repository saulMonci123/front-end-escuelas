import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla alumnos de la base de datos
 */
@Entity()
export class Alumnos extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column()
    id_nivel: number;

    @Column({
        type:"int"
    })
    id_persona: number;

    @Column()
    tipoSangre: number;
    
    @Column()
    matricula: string;

    @Column({
        type:"date"
    })
    fecha_nacimiento: string;

    @Column()
    lugar_nac: string;

    @Column()
    escuela_ant: string;

    @Column()
    clave_seguro: string;

    @Column()
    tipo_seguro: string;

    @Column()
    curp: string;

    @Column()
    actividad: string;
    
    @Column()
    impedimento: string;

    @Column()
    algo_mas: string;

    @Column()
    grado: Number;

    @Column()
    grupo: string;

    @Column()
    especialidad: string;

    @Column()
    cuatrimestre: Number;

    @Column()
    modalidad: Number;

    @Column()
    status: Boolean;

    @Column()
    id_persona_tutor: Number;

    @Column()
    id_persona_paga: Number;

    @Column()
    id_persona_fiscal: Number;

    @Column({
        type:"float"
    })
    transporte_ida: Number;

    @Column({
        type:"float"
    })
    transporte_vuelta: Number;

    @Column({
        type:"float"
    })
    transporte_monto:Number;

    @Column()
    ruta: string;

    @Column({
        type:"int2"
    })
    alimentos_am: Number;

    @Column({
        type:"int2"
    })
    alimentos_pm: Number;

    @Column({
        type:"numeric"
    })
    alimentos_monto:Number;

    @Column({
        type:"int2"
    })
    estancia_antes: number;

    @Column({
        type:"numeric"
    })
    estancia_antes_monto:number;

    @Column({
        type:"int2"
    })
    estancia_despues:number;

    @Column({
        type:"numeric"
    })
    estancia_despues_monto:number;

    @Column({
        type:"numeric"
    })
    mensualidad:number;

    @Column({
        type:"numeric"
    })
    multa:number;

    @Column({
        type:"numeric"
    })
    beca_porc:number;

    @Column({
        type:"varchar"
    })
    beca_motivo:string;

    @Column({
        type:"numeric"
    })
    inscripcion:number;

    @Column({
        type:"timestamp"
    })
    fecha_inscripcion:string;

    @Column({
        type:"numeric"
    })
    cuota_padres:number;

    @Column({
        type:"timestamp"
    })
    fecha_almacenado:string;

    @Column({
        type:"varchar"
    })
    solicitud:string;

    @Column({
        type:"varchar"
    })
    acta_nacimiento:string;

    @Column({
        type:"varchar"
    })
    cartilla_vacunacion:string;

    @Column({
        type:"varchar"
    })
    certif_preescolar:string;

    @Column({
        type:"varchar"
    })
    certif_primaria:string;

    @Column({
        type:"varchar"
    })
    certif_secundaria:string;

    @Column({
        type:"varchar"
    })
    certif_bachillerato:string;

    @Column({
        type:"varchar"
    })
    certif_universidad:string;

    @Column({
        type:"varchar"
    })
    fotografias:string;

    @Column({
        type:"varchar"
    })
    curp_impresa:string;

    @Column({
        type:"varchar"
    })
    status_alumno:string;

    @Column({
        type:"varchar"
    })
    realiza_servicio:string;

    @Column({
        type:"varchar"
    })
    servicio_liberado:string;

    @Column({
        type:"varchar"
    })
    practicas_liberadas:string;

    @Column({
        type:"int2"
    })
    anio_egreso:number;

    @Column({
        type:"numeric"
    })
    beca_cuota:number;

    @Column({
        type:"int4"
    })
    dia_vencimiento:number;

    @Column({
        type:"varchar"
    })
    como_se_entero:string;

    @Column({
        type:"varchar"
    })
    domic_escuela_proced:string;

    @Column({
        type:"varchar"
    })
    fecha_egreso_escuela_proced:string;

    @Column({
        type:"int2"
    })
    id_user:number;

    @Column({
        type:"varchar"
    })
    estado_nacimiento:string;

    @Column({
        type:"date"
    })
    fecha_egreso:string;

    @Column({
        type:"int"
    })
    id_empresa:number;

    @Column({
        type:"int"
    })
    id_campus:number;
}