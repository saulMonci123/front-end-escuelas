import {Entity, Column, PrimaryGeneratedColumn, BaseEntity} from "typeorm";
import bcrypt from 'bcryptjs';



/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column()
    username: string;

    @Column()
    email: string;
    
    
    @Column()
    password: string;


    async encrytpPassword(password: string): Promise<string>{
        const salt = await bcrypt.genSalt(10);
        return bcrypt.hash(password, salt);
    }

    async validatePassword(password: string):Promise<boolean>{
        return await bcrypt.compare(password, this.password);
    }
}