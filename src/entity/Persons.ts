import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Personas extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

   @Column({
       type:"varchar"
   })
   nombre: string;

   @Column({
    type:"varchar"
   })
   paterno: string;

   @Column({
    type:"varchar"
   })
   materno: string;

   @Column({
    type:"int"
   })
   sexo: number;

   @Column({
    type:"varchar"
   })
   curp: string;

   @Column({
    type:"varchar"
   })
   calle: string;

   @Column({
    type:"varchar"
   })
   numero_exterior: string;

   @Column({
    type:"varchar"
   })
   numero_interior: string;

   @Column({
    type:"varchar"
   })
   colonia: string;

   @Column({
    type:"varchar"
   })
   cp: string;

   @Column({
    type:"varchar"
   })
   poblacion: string;

   @Column({
    type:"varchar"
   })
   municipio: string;

   @Column({
    type:"varchar"
   })
   estado: string;

   @Column({
    type:"varchar"
   })
   telefono: string;

   @Column({
    type:"varchar"
   })
   email: string;

   @Column({
    type:"boolean"
   })
   estatus: boolean;

   @Column({
    type:"varchar"
   })
   rfc: string;

   @Column({
    type:"varchar"
   })
   lugar_laboral: string;

   @Column({
    type:"varchar"
   })
   ocupacion: string;

   @Column({
    type:"float"
   })
   sueldo_mensual: number;

   @Column({
    type:"varchar"
   })
   razonsocial: string;

   @Column({
    type:"varchar"
   })
   estudios: string;

   @Column({
    type:"varchar"
   })
   telefono_celular: string;

   @Column({
    type:"int"
   })
   id_user: number;

   @Column({
    type:"varchar"
   })
   ine: string;

   @Column({
    type:"varchar"
   })
   pasaporte: string;

   @Column({
    type:"varchar"
   })
   fisica_moral: string;

   @Column({
    type:"int"
   })
   id_empresa: number;

   @Column({
    type:"int"
   })
   id_pais: number;
}