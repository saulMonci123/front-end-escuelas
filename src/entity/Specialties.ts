import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Especialidades extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column({
        type:"varchar"
    })
    nombre: string;

    @Column({
        type:"int"
    })
    id_empresa:number;

    @Column({
        type:"int"
    })
    id_campus:number;

    @Column({
        type:"int"
    })
    id_nivel: number;

    @Column({
        type:"boolean"
    })
    status: boolean;
    

}