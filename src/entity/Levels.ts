import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Double} from "typeorm";

/**
 * Modelo de la tabla users de la base de datos
 */
@Entity()
export class Niveles extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: Number;

    @Column({
        type:"varchar"
    })
    nivel: string;

    @Column({
        type:"varchar"
    })
    ciclo: string;

    @Column({
        type:"int"
    })
    anio_inicio: number;

    @Column({
        type:"int"
    })
    mes_inicio: number;

    @Column({
        type:"int"
    })
    anio_fin: number;

    @Column({
        type:"int"
    })
    mes_fin: number;

    @Column({
        type:"int"
    })
    transporte_monto: number;

    @Column({
        type:"int"
    })
    alimentos_monto: number;

    @Column({
        type:"int"
    })
    estancia_antes_monto: number;

    @Column({
        type:"int"
    })
    estancia_despues_monto: number;

    @Column({
        type:"int"
    })
    mensualidad: number;

    @Column({
        type:"int"
    })
    multa: number;

    @Column({
        type:"int"
    })
    inscripcion: number;

    @Column({
        type:"int"
    })
    cuota_padres: number;

    @Column({
        type:"timestamp"
    })
    fecha_almacenado: string;

    @Column({
        type:"int"
    })
    id_empresa: number;

    @Column({
        type:"int"
    })
    id_campus: number;








}