import dotenv from 'dotenv';
import app from "./app";
import {createConnection} from 'typeorm';
import routes from "./routes/routes";
import http from 'http';
import database from "./database";
dotenv.config();



async function main(){    
    await createConnection(database);
    
    const server = app.listen(app.get('port'));

    var httpServer = http.createServer().listen(4001);
    //Socket
    
    //routes
    app.use(routes);

    console.log('Server on port ', app.get('port'));
}

main();
