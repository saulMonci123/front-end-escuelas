import {ConnectionOptions} from "typeorm";
import dotenv from "dotenv";
dotenv.config();

const {DB_USERNAME, DB_PASSWORD, DB_DATABASE} = process.env



/**
 * Configuracion para la conexion a la base de datos
 */
const database: ConnectionOptions = {
    type: 'postgres',
    host: '201.144.13.199',
    port:  5432,
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_DATABASE,
    synchronize: true,
    logging: false,
    connectTimeout: 60000,
    acquireTimeout: 60 * 60 * 1000,
    entities: [
        "entity/*.js",
    ],
    cli: {
        "migrationsDir": "src/migrations"
    }
  };

export default database;