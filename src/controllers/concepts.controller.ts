
import { Conceptos } from '../entity/Concepts';
import {Request, Response, NextFunction} from 'express';
import { getRepository } from 'typeorm';
import { Pagos } from '../entity/Pays';
import { TiposPagos } from '../entity/TypePays';
import { Cajeros } from '../entity/Cashier';
import { PagosDetalles } from '../entity/PaysDetails';
import { asyncForEach } from '../libs/utils';


export default class ConceptsController {

    async getNivels(req:Request, res:Response, next:NextFunction){
        const conceptsRepository = await getRepository(Conceptos)
        const data = await conceptsRepository.find();
        return res.status(200).json(data);
    }       


    async generatePay(req:Request, res:Response, next:NextFunction){
        try {
            const {
                cantidad,
                conceptos,
                alumno,
                tipoPago
            } = req.body;

            console.log('Estos son los conceptos', conceptos);
            const cajerosRepository = await getRepository(Cajeros);

            const data = await cajerosRepository.findOne({
                where:{
                    id_usuario:req.userId
                }
            });

            let recibo: number = 1;        
            if(data?.recibo !== null){
                recibo = data?.recibo + 1;
            }

            data?.recibo = recibo;

            data?.save();

            const pay = new Pagos;

            pay.id_user = req.userId;
            pay.id_alumno = alumno;
            pay.recibo = recibo;
            pay.fecha_pago = new Date();

            const newPay = await pay.save();



            asyncForEach(conceptos, async (concepto)=>{
                const payDetail = new PagosDetalles;
                payDetail.id_pago = newPay.id;
                payDetail.id_concepto = concepto.id;
                payDetail.id_tipopago = tipoPago;
                payDetail.monto_paga = concepto.costo;
    
                await payDetail.save();
            });
           
            return res.status(200).json({message:"Success"});
        } catch (error) {
            console.log(error);
            return res.status(400).json({mesage:"Error"});
        }
    }


    async getTypesPays(req:Request, res:Response, next:NextFunction){
        const tiposPagosRepository = getRepository(TiposPagos);
        const data = await tiposPagosRepository.find();
        return res.status(200).json(data);
    }

}