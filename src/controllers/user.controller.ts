import {Request, Response, NextFunction} from 'express';
import {User} from '../entity/User';
import {getRepository, getConnection} from 'typeorm';




export default class UserController {


    /**
     * Retorna todos los usuarios de la base de datos
     * @param req 
     * @param res 
     * @param next 
     */
    public async index(req:Request, res:Response, next:NextFunction){
        try{
            const userRepository = await getRepository(User);
            const users =  await userRepository.find();
            return res.json(users);
        }catch(e){
            next(e)
        }
    }

}