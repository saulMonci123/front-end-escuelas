import {Request, Response, NextFunction} from 'express';
import {User} from '../entity/User';
import {getRepository, getConnection} from 'typeorm';
import jwt from 'jsonwebtoken';
import doten from "dotenv";
doten.config();

export default class AuthController {

    /**
     * Registra un usuario nuevo
     * @param req 
     * @param res 
     * @param next 
     */
    public async signup(req:Request, res:Response, next:NextFunction){
        try{
            //Savin user
            const user = new User();
            user.username = req.body.username;
            user.email = req.body.email;
            user.password = req.body.password;
            user.password =  await user.encrytpPassword(user.password);
            await user.save()

            //token
            const token: string = jwt.sign({_id:user.id}, <string>process.env.APP_TOKEN);

            return res.header('auth-token', token).json({message:'User Saved Succefully', user});
        }catch(e){
            next(e)
        }
       
    }
    

    /**
     * Funcion que valida la informacion del usuario para iniciar sesion
     * 
     * @param req 
     * @param res 
     * @param next 
     * 
     * **Pseudocodigo**
     * 
     *  Se valida que el usuario exista en la base de datos
     * 
     * Se valida que la constraseña sea la correcta
     * 
     * En caso de que los datos sean validos se retornara el token al cliente
     * 
     */
    public async signin(req:Request, res:Response, next:NextFunction){
        try{
             //Savin user
             const repository = await  getRepository(User);
             const user = await repository.findOne({email:req.body.email});
             
             //validate user
             if(!user){
                res.status(400).json('Email or user is wrong');
             }

             //validate password
             const correctPassword = await user?.validatePassword(req.body.password);
             if(!correctPassword){
                res.status(400).json('Invalid password');
             }

            //token
            const token: string = jwt.sign({_id:user?.id}, <string>process.env.APP_TOKEN,{
                expiresIn: 86400
            });

            return res.status(200).json({token:token,user});
        }catch(e){
            next(e)
        }
    }
    
    public async profile(req:Request, res:Response, next:NextFunction){
        try{
            const repository = await getRepository(User);
            const user = await repository.findOne({id:req.userId});
            if(!user) return res.status(404).json('No user found')
            return res.json(user);
        }catch(e){
            next(e)
        }
       
    }

}