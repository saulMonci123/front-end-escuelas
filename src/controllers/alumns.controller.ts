import {Request, Response, NextFunction} from 'express';
import {User} from '../entity/User';
import {getRepository, getConnection} from 'typeorm';
import jwt from 'jsonwebtoken';
import doten from "dotenv";
import { Alumnos } from '../entity/Alumns';
import { Personas } from '../entity/Persons';
import { Niveles } from '../entity/Levels';
doten.config();

export default class AlumnsController {

    /**
     * Busca un alumno para la colegiatura
     * @param req 
     * @param res 
     * @param next 
     */
    public async getAlumn(req:Request, res:Response, next:NextFunction){
        try{
            const {matricula, nombre, apellido_paterno, apellido_materno} = req.body;

            const alumnsRepository = getRepository(Alumnos)
            const query = await alumnsRepository.createQueryBuilder('alumnos')
                        .innerJoinAndMapOne("alumnos.persona",Personas, "personas", "alumnos.id_persona = personas.id")
                        .innerJoinAndMapOne("alumnos.nivel", Niveles, "niveles", "alumnos.id_nivel = niveles.id"); 


            if(matricula !== ""){
                query.where("alumnos.matricula = :matricula" ,{matricula:matricula})
            }
            if(nombre !== ""){
                query.where("personas.nombre like :nombre", {nombre:`%${nombre}%`})
            }
            
            if(apellido_materno !== ""){
                query.where("personas.materno = :materno", {materno:apellido_materno})
            }

            if(apellido_paterno !== ""){
                query.where("personas.paterno = :paterno", {paterno:apellido_paterno})
            }

            const data = await query.getMany();
            
            const arrayToSend:any[] = [];            
            data.map((d)=>{
                arrayToSend.push({
                    id:d.id,
                    matricula:d.matricula,
                    nombre: d.persona.nombre + " "+ d.persona.paterno + " " + d.persona.materno ,
                    escolaridad: d.nivel.nivel,
                    grupo: d.grupo,
                    grado: d.grado,
                    especialidad: d.especialidad
                });
            });            
            return res.status(200).json(arrayToSend);
        }catch(e){
            next(e)
        }
       
    }
}